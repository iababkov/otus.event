﻿using System;
using System.Threading;
using OtusEventLibrary;

namespace OtusEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            string targetDirectory = getDir(Environment.SpecialFolder.MyDocuments);
            uint timeOut = getTimeOut(15);

            DocumentsReceiver myReceiver = null;
            try
            {
                myReceiver = new(targetDirectory, timeOut);
                myReceiver.AddFileToWatch("Паспорт.jpg");
                myReceiver.AddFileToWatch("Заявление.txt");
                myReceiver.AddFileToWatch("Фото.jpg");
                myReceiver.DocumentsReady += () => Console.WriteLine("Все документы загружены.");
                myReceiver.TimedOut += () => Console.WriteLine("Время истекло!");

                Console.WriteLine("Время пошло. Для отмены нажмите Ctrl+С.");

                myReceiver.Start();

                while (myReceiver.KeepWatching)
                {
                    Thread.Sleep(500);
                }
            }
            catch
            {
                if (myReceiver != null)
                {
                    myReceiver.Dispose();
                }
            }
        }

        static string getDir(Environment.SpecialFolder defaultSpecialFolder)
        {
            string defaultDir = Environment.GetFolderPath(defaultSpecialFolder);

            Console.WriteLine($"Укажите директорию и/или нажмите Enter (по умолчанию: {defaultDir})");

            string dir = Console.ReadLine();

            return dir == "" ? defaultDir : dir;
        }

        static uint getTimeOut(uint defaultTimeOut)
        {
            Console.WriteLine($"Укажите задержку в секундах и/или нажмите Enter (по умолчанию: {defaultTimeOut})");

            _ = uint.TryParse(Console.ReadLine(), out uint timeOut);

            return timeOut == 0 ? defaultTimeOut : timeOut;
        }
    }
}
